
ENGLISH

SRT to JSON converter
Version 0.2

This console utility is designed to convert SRT subtitles to JSON format

Command Line Arguments
-st input file with subtitles in SRT format
-o output file with JSON
-ntt convert numbers into their names (in the current version only in Russian)
-[] remove text in square brackets

Command line example:
SRTtoJSON -st"you_SRT_file.srt" -o"out_json_file.json"

The output file contains an array, each element of which contains the following fields:
Index - subtitle number
Text - subtitle text
SourceTime - time, in the format used in SRT subtitles
StartTime - start time of displaying subtitle text in hh:mm:ss:zzz format
EndTime - end time of displaying subtitle text in hh:mm:ss:zzz format
StartTimeMs - start time of displaying subtitle text in milliseconds
EndTimeMs - end time of subtitle text display in milliseconds

Attention ! ! !
The StartTime, EndTime, StartTimeMs, and EndTimeMs fields are calculated and converted from the SourceTime field.

Updates in version 0.2
Add:
- Added the ability to convert numbers into their verbal names (writing numbers in letters), but only for the Russian language
- Added the ability to delete text in square brackets
Fix:
- Fixed minor non-critical bugs



RUSSIAN / РУССКИЙ

SRT to JSON convertor
Версия 0.1

Данная консольная утилита предназначена для конвертации субтитров формата SRT в формат JSON

Аргументы командной строки
-st входной файл с субтитрами в формате SRT
-o выходной файл с JSON
-ntt конвертировать цифры в их названия (записать цифры буквами) (в текущей версии доступен только русский язык)
-[] удаление текста в квадратных скобках

Пример командной строки:
SRTtoJSON -st"you_SRT_file.srt" -o"out_json_file.json"

Выходной файл содердит масив, каждый элемент которого содержит следующие поля:
Index - номер субтитров
Text - текст субтитров
SourseTime - время, в том формате, который используется в SRT субтитрах
StartTime - время начала показа текста субтитров в формате hh:mm:ss:zzz
EndTime - время конца показа текста субтитров в формате hh:mm:ss:zzz
StartTimeMs - время начала показа текста субтитров в милесекундах
EndTimeMs - время конца показа текста субтитров в милесекундах

Внимание ! ! !
Поля StartTime, EndTime, StartTimeMs и EndTimeMs являются вычесляемыми и преобразуются из поля SourseTime.

Обновления в версии 0.2
Добавлено:
- Добавлена возможность преобразовывать цифры в их словетсные названия (запись цифр буквами), но только для русского языка
- Добавлена возможность удалять текст в квадратных скобках
Исправления:
- Исправлены мелкие не критичные баги