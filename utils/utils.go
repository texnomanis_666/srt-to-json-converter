package utils

import (
	"regexp"
	"strings"
)

func RemoveTextBetweenSquareBrackets(s string) string {
	// Создаем регулярное выражение для поиска текста между квадратными скобками
	re := regexp.MustCompile(`\[[^\[\]]*\]`)

	// Заменяем все найденные совпадения на пустую строку
	cleanedStr := re.ReplaceAllString(s, "")

	return cleanedStr
}

func RemoveTextAfterFirstSquareBracket(s string) string {
	// Ищем индекс первой квадратной скобки
	index := strings.Index(s, "[")

	if index != -1 {
		// Обрезаем строку до индекса первой квадратной скобки
		cleanedStr := s[:index]

		return cleanedStr
	}

	return s
}

func RemoveTextBeforeFirstSquareBracket(s string) string {
	// Ищем индекс первой квадратной скобки
	index := strings.Index(s, "[")

	if index != -1 {
		// Обрезаем строку после индекса первой квадратной скобки
		cleanedStr := s[index:]

		return cleanedStr
	}

	return s
}
