package main

import (
	ntt "SrtToJSON/NumbersToText"
	"SrtToJSON/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type Subtitle struct {
	Index       string
	SourseTime  string
	StartTime   string
	EndTime     string
	StartTimeMs string
	EndTimeMs   string
	Text        string
}

func main() {

	var i, u int
	var Subtitles []Subtitle

	fmt.Println("SRT to JSON converter")
	fmt.Println("Version 0.2")
	fmt.Println("by TexnomaniS")
	fmt.Println("-st входной файл субтитров SRT")
	fmt.Println("-o выходной файл JSON")
	fmt.Println("-ntt записать цифры буквами (конвертировать числа в их названия)")
	fmt.Println("-[] Удалить текст в квадратных скобках")
	fmt.Println("Сканирование аргументов командной строки")

	substr_file := ""
	out_file := ""
	number_to_text := false
	rsb := false //удалять всё что в квадратных скобках

	// читаем командную строку
	for i = 0; i < len(os.Args); i++ {

		if strings.Index(os.Args[i], "-st") == 0 {
			substr_file = os.Args[i]
			substr_file = substr_file[3:]
		}

		if strings.Index(os.Args[i], "-o") == 0 {
			out_file = os.Args[i]
			out_file = out_file[2:]
		}

		if strings.Index(os.Args[i], "-ntt") == 0 {
			number_to_text = true
		}

		if strings.Index(os.Args[i], "-[]") == 0 {
			rsb = true
		}

	}

	if substr_file == "" {
		fmt.Println("Не указан файл с субтитрами")
		os.Exit(0)
	}

	// воткнуть проверку на наличие исходного файла и нулевые аргументы

	fmt.Println("Начало парсинга субтитров")
	fmt.Println("Читаем файл " + substr_file)

	// Читаем содержимое файла
	data, err := ioutil.ReadFile(substr_file)
	if err != nil {
		fmt.Println(err)
	}

	// delete BOM simbols
	bom := []byte{0xEF, 0xBB, 0xBF}
	if len(data) >= 3 && data[0] == bom[0] && data[1] == bom[1] && data[2] == bom[2] {
		data = data[3:]
	}

	var subtitles_arr []string
	var one_arr []string
	var st Subtitle
	var str string
	var time_arr []string

	// все спецсимволы перехода на след. строку должны быть одинаковыми
	str = strings.ReplaceAll(
		string(data),
		string(byte(13))+string(byte(10)),
		string(byte(10)))

	// спецсимволов перехода на след. строку не должно идти больше 2-х подряд
	for strings.Count(str, string(byte(10))+string(byte(10))+string(byte(10))) > 0 {
		str = strings.ReplaceAll(
			str,
			string(byte(10))+string(byte(10))+string(byte(10)),
			string(byte(10)))
	}

	subtitles_arr = strings.Split(str, string(byte(10))+string(byte(10)))

	for i = 0; i < len(subtitles_arr)-1; i++ {

		// убираем переходы на следующую строку перед началом строки
		str = strings.TrimPrefix(subtitles_arr[i], string(byte(10)))
		// делим на масив испоользуя в качестве разделителя символ перехода на следующую строку
		one_arr = strings.Split(str, string(byte(10)))

		if one_arr[0] != "" && len(one_arr) > 0 {
			st.Index = one_arr[0]

			// get start & end time
			st.SourseTime = one_arr[1]
			// проверяем что время записалось как время
			if !strings.Contains(st.SourseTime, "-->") {
				continue
			}

			time_arr = strings.Split(st.SourseTime, "-->")
			st.StartTime = time_arr[0]
			st.EndTime = time_arr[1]
			st.StartTime = strings.ReplaceAll(st.StartTime, " ", "")
			st.EndTime = strings.ReplaceAll(st.EndTime, " ", "")

			// time convertion to miliseconds
			time_h, _ := strconv.Atoi(st.StartTime[:2])
			time_m, _ := strconv.Atoi(st.StartTime[3:5])
			time_s, _ := strconv.Atoi(st.StartTime[6:8])
			time_ms, _ := strconv.Atoi(st.StartTime[9:12])
			st.StartTimeMs = strconv.Itoa(time_ms + (time_s * 1000) + (time_m * 1000 * 60) + (time_h * 1000 * 60 * 60))

			time_h, _ = strconv.Atoi(st.EndTime[:2])
			time_m, _ = strconv.Atoi(st.EndTime[3:5])
			time_s, _ = strconv.Atoi(st.EndTime[6:8])
			time_ms, _ = strconv.Atoi(st.EndTime[9:12])
			st.EndTimeMs = strconv.Itoa(time_ms + (time_s * 1000) + (time_m * 1000 * 60) + (time_h * 1000 * 60 * 60))

			// get all text strings
			st.Text = one_arr[2]
			if len(one_arr) > 3 {
				for u = 3; u < len(one_arr); u++ {
					st.Text = st.Text + one_arr[u]
				}
			}

			if number_to_text == true {

				var number, number_text string

				if ntt.HaveNumbers(st.Text) {
					number = ntt.ExtractNumberFromString(st.Text)
					number_text = ntt.ConvertToText_RUS(number)
					st.Text = strings.Replace(st.Text, number, number_text, 1)

				}

			}

			if rsb == true {

				st.Text = utils.RemoveTextBetweenSquareBrackets(st.Text)
				st.Text = utils.RemoveTextAfterFirstSquareBracket(st.Text)
				st.Text = utils.RemoveTextBeforeFirstSquareBracket(st.Text)

			}

			if st.Text != "" && !strings.Contains(st.Text, "-->") {
				Subtitles = append(Subtitles, st)
			}
		}

	}

	// записывем цифры буквами (преобразуем числа в их названия)

	fmt.Println("Парсинг успешно завершён")
	fmt.Println("Конвертация в JSON")

	jsonData, err := json.Marshal(Subtitles)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Сохранение в файл", out_file)
	err = ioutil.WriteFile(out_file, []byte(jsonData), 0777)
	if err != nil {
		fmt.Println(err)
	}
}
