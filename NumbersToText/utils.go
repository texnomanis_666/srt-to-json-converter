package NumbersToText

import "unicode"

func HaveNumbers(s string) bool {
	for _, char := range s {
		if unicode.IsDigit(char) {
			return true
		}
	}
	return false
}

func ExtractNumberFromString(str string) string {
	rez := ""
	var naidena byte

	naidena = 1

	if HaveNumbers(str) == false {
		return str
	} else {

		for i := 0; i < len(str); i++ {

			if HaveNumbers(string(str[i])) == true {
				naidena = 0

			}

			if HaveNumbers(string(str[i])) == false && naidena == 0 {
				naidena = 2
				break
			}

			if naidena == 0 {
				rez = rez + string(str[i])
			}
		}
	}
	return rez
}

/*
example of use


			if jedai.CifraExissts(text) == true {

				for c := true; c; c = jedai.CifraExissts(text) {
					number = jedai.ExtractNumberFromString(text)
					number_text = jedai.ParseMinimal(number)
					text = strings.Replace(text, number, number_text, 1)
				}


*/
