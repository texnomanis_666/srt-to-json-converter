package NumbersToText

/*
Модуль в который собраны  функции для преобразования чисел в их буквенные названия



*/

import (
	"strings"
)

func parseOneCifra_RUS(str string) string {
	str = strings.ToUpper(str)

	if str == "1" {
		return " один "
	}
	if str == "2" {
		return " два "
	}

	if str == "3" {
		return " три "
	}

	if str == "4" {
		return " четыре "
	}

	if str == "5" {
		return " пять "
	}

	if str == "6" {
		return " шесть "
	}

	if str == "7" {
		return " семь "
	}

	if str == "8" {
		return " восемь "
	}

	if str == "9" {
		return " девять "
	}

	if str == "0" {
		return " ноль "
	}

	return " "
}

func parseTwoCifra_RUS(str string) string {
	//str = strings.ToUpper(str)

	rez := ""

	//if str[0] == "0" {
	//	return parseOneCifra_RUS(str[1])
	//}

	if string(str[0]) == "9" {
		rez = " девяносто "
	}

	if string(str[0]) == "8" {
		rez = " восемьдесят "
	}

	if string(str[0]) == "7" {
		rez = " семьдесят "
	}

	if string(str[0]) == "6" {
		rez = " шестьдесят "
	}

	if string(str[0]) == "5" {
		rez = " пятьдесят "
	}

	if string(str[0]) == "4" {
		rez = " сорок "
	}
	if string(str[0]) == "3" {
		rez = " тридцать "
	}
	if string(str[0]) == "2" {
		rez = " двадцать "
	}

	if string(str[0]) == "1" {

		if string(str[1]) == "0" {
			rez = " десять "
		}

		if string(str[1]) == "1" {
			rez = " одинадцать "
		}

		if string(str[1]) == "2" {
			rez = " двенадцать "
		}

		if string(str[1]) == "3" {
			rez = " тринадцать "
		}

		if string(str[1]) == "4" {
			rez = " четырнадцать "
		}

		if string(str[1]) == "5" {
			rez = " пятнадцать "
		}

		if string(str[1]) == "6" {
			rez = " шестнадцать "
		}

		if string(str[1]) == "7" {
			rez = " семьнадцать "
		}

		if string(str[1]) == "8" {
			rez = " восемьнадцать "
		}

		if string(str[1]) == "9" {
			rez = " девятнадцать "
		}

		return rez
	}

	if string(str[1]) != "0" {
		rez = rez + parseOneCifra_RUS(string(str[1]))
	}

	return rez
}

func parseTriCifra_RUS(str string) string {
	//str = strings.ToUpper(str)

	rez := ""

	if string(str[0]) == "0" {
		rez = "  "
	}

	if string(str[0]) == "9" {
		rez = " девятьсот "
	}

	if string(str[0]) == "8" {
		rez = " восемьсот "
	}

	if string(str[0]) == "7" {
		rez = " семьсот "
	}

	if string(str[0]) == "6" {
		rez = " шестьсот "
	}

	if string(str[0]) == "5" {
		rez = " пятьсот "
	}

	if string(str[0]) == "4" {
		rez = " четыреста "
	}

	if string(str[0]) == "3" {
		rez = " триста "
	}

	if string(str[0]) == "2" {
		rez = " двести "
	}

	if string(str[0]) == "1" {
		rez = " сто "
	}

	if string(str[1]) != "0" {
		rez = rez + parseTwoCifra_RUS(str[1:len(str)])
	}

	if string(str[1]) == "0" && string(str[2]) != "0" {
		rez = rez + parseOneCifra_RUS(str[2:len(str)])
	}

	return rez
}

func parseFourCifra_RUS(str string) string {
	//str = strings.ToUpper(str)

	rez := ""

	if string(str[0]) == "1" {
		rez = " тысяча "
	}

	if string(str[0]) == "2" {
		rez = " две тысячи "
	}

	if string(str[0]) == "3" {
		rez = " три тысячи "
	}

	if string(str[0]) == "4" {
		rez = " четыре тысячи "
	}

	if string(str[0]) == "5" {
		rez = " пять тысячь "
	}

	if string(str[0]) == "6" {
		rez = " шесть тысячь "
	}

	if string(str[0]) == "7" {
		rez = " семь тысячь "
	}

	if string(str[0]) == "8" {
		rez = " восемь тысячь "
	}

	if string(str[0]) == "9" {
		rez = " девять тысячь "
	}

	rez = rez + parseTriCifra_RUS(str[1:len(str)])

	return rez
}

func ConvertToText_RUS(str string) string {
	if len(str) == 1 {
		return parseOneCifra_RUS(str)
	}

	if len(str) == 2 {
		return parseTwoCifra_RUS(str)
	}

	if len(str) == 3 {
		return parseTriCifra_RUS(str)
	}

	if len(str) == 4 {
		return parseFourCifra_RUS(str)
	}

	if len(str) > 4 {
		return str
	}

	return ""
}
